'use strict';

const { Component, h, render } = preact

function request(method, url, params, onsuccess, onerror) {
  var xhr = new XMLHttpRequest()
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      onsuccess && onsuccess(JSON.parse(xhr.responseText))
    }
  }
  if (onerror) xhr.onerror = onerror
  xhr.open(method, url, true)
  xhr.send(params && JSON.stringify(params) || null)
}

class CommentBox extends Component {

  loadCommentsFromServer() {
    request("GET", this.props.url, null, function(response) {
      this.setState({data: response})
    }.bind(this))
  }

  handleCommentSubmit(comment) {
    var comments = this.state.data
    comment.id = Date.now()
    var newComments = comments.concat([comment])
    this.setState({data: newComments})
    request('POST', this.props.url, comment, function(response) {
      this.setState({data: response})
    }.bind(this), function(error) {
      this.setState({data: comments})
      console.error(this.props.url, error.toString())
    })
  }

  getInitialState() {
    return {data: []}
  }

  componentDidMount() {
    this.loadCommentsFromServer()
    // setInterval(this.loadCommentsFromServer.bind(this), this.props.pollInterval)
  }

  render(props, state) {
    return (
      <div className="comment-box">
        <h1>Comments</h1>
        <CommentList data={this.state.data} />
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
    )
  }
}

class CommentList extends Component {

  render() {
    var commentNodes = this.props.data.map(function(comment, i) {
      return h(Comment, {
        author: comment.author,
        key: comment.id,
        avatar: comment.avatar
      }, comment.text)
    })
    return h("div", {className: "comment-list"}, commentNodes)
  }
}

class CommentForm extends Component {

  getInitialState() {
    return {author: '', text: ''}
  }

  handleAuthorChange(e) {
    this.setState({author: e.target.value})
  }

  handleTextChange(e) {
    this.setState({text: e.target.value})
  }

  handleSubmit(e) {
    e.preventDefault()
    var author = this.state.author.trim()
    var text = this.state.text.trim()
    if (!text || !author) {
      return
    }
    this.props.onCommentSubmit({author: author, text: text})
    this.setState({author: '', text: ''})
  }

  render() {
    return h("div", {className: "comment-form"},
      h("h2", null, "Leave a comment"),
      h("form", {onSubmit: this.handleSubmit.bind(this)},
        h("label", null, "Name",
          h("input", {
            value: this.state.author,
            onChange: this.handleAuthorChange.bind(this)
          })
        ),
        h("label", null, "Comment",
          h("textarea", {
            value: this.state.text,
            onChange: this.handleTextChange.bind(this)
          })
        ),
        h("button", null, "Send")
      )
    )
  }
}

class Comment extends Component {

  rawMarkup() {
    var md = new Showdown.converter();
    var rawMarkup = md.makeHtml(this.props.children.toString());
    return { __html: rawMarkup };
  }

  render(props, state) {
    return h("div", {className: "comment"},
      h("div", {className: "content", dangerouslySetInnerHTML: this.rawMarkup()}),
      h("div", {className: "author"}, this.props.author)
    )
  }
}

render(
  h(CommentBox, {url: "../api/comments/", pollInterval: 5000}),
  document.body.querySelector('.container')
)