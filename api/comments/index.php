<?php

$filename = 'comments.json';

$from_client = file_get_contents('php://input');

if ($from_client) {
  $data = json_decode($from_client, true);
  $comments = json_decode(file_get_contents($filename), true);
  $comments[] = [
    'id' => $comments[count($comments) - 1]['id'] + 1,
    'avatar' => 'assets/img/1.jpg',
    'author' => $data['author'],
    'text' => $data['text']
  ];
  $json = json_encode($comments, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
  file_put_contents($filename, $json);
  print $json;
} else {
  print file_get_contents($filename);
}

?>